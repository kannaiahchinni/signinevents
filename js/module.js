(function (angular) {
   
    'use strict';
    
    /* Angular module declaration here */
    angular.module('signinApp', ['ngRoute','ngMaterial']);
    
})(window.angular);