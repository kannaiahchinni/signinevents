(function(angular){
    
    'use strict';
    
    
    var Service = function($http){
        
        return {
            
            getImages: function(){
                return  $http.get('json/images.json'); //$http.get('http://signinevents.com/getData.php?cat=image');
            },
            getVideos : function(){
                return $http.get('json/videos.json'); //$http.get('http://signinevents.com/getData.php?cat=video')
            }
        }
        
        
    };
    
    Service.inject = ['$http'];
    
    angular.module('signinApp').factory('Service',Service);
//       
//        return {
//            
//            getImageData: function(){
//                var data = new FormData();
//                
//                
//            }
//        }
//        
//        
//    });
    
    
})(window.angular);