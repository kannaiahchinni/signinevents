(function (angular) {
    
    'use strict';
    
   var configuration = function ($routeProvider, $locationProvider,$sceDelegateProvider) {
        
       $locationProvider.hashPrefix('!');
        $routeProvider.when('/services', {
            templateUrl: 'views/services.html',
            controller:'homeCtrl as hCtrl'
        }).when('/gallery', {
            templateUrl: 'views/gallery.html',
            controller: 'ImageCtrl'
        }).when('/contact', {
            templateUrl: 'views/contact.html',
            controller: 'ContactCtrl'
        }).when('/about', {
            templateUrl: 'views/about.html'
        }).when('/', {
            templateUrl: 'views/home.html'
        }).when('/videos', {
            templateUrl: 'views/videos.html'
        }).otherwise('/');
       
       $sceDelegateProvider.resourceUrlWhitelist([
            'self',
           'https://www.youtube.com/**',
           'http://signinevents.com/**'
       ]);
       
    };
    
    configuration.inject = ['$routeProvider','$locationProvider','$sceDelegateProvider'];
    angular.module('signinApp').config(configuration);
//    angular.module('signinApp').config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider){
//        
//    }]);
    
})(window.angular);