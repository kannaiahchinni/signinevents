(function(angular){
    'use strict';
    
    var imageController = function($scope,Service,$log,$mdDialog){
        'use strict'
        
        var imageCC = this;
        
        imageCC.imageList = [];
        
        Service.getImages().success(function(data){
            imageCC.imageList = data;
            $log.info("image data "+ data);
            
        }).error(function(){
           console.log('error occured while getting image data '); 
        });
        
        this.showViewer = function(object){
            $log.info("image name is "+ object.imagename);
            $mdDialog.show({
                template: '<md-dialog aria-label=""  ng-cloak>'+
                        '<form>'+
                        '<md-dialog-content><div class="md-dialog-content">'+
                        '<md-card ><div class="md-card dialog">'+
                        '<img ng-src="images/'+object.imagename+'" class="imagee" alt="Signinevents image, event mangagement in Hyderabad."></div></md-card>'+
                        '</div>'+
                        '</md-dialog-content><md-dialog-actions layout="row"><md-button ng-click="close(\'close\')">Close</md-button></md-dialog-actions></form></md-dialog>',
                parent: angular.element(document.body),
                clickOutsideToClose:true,
            }).then(function(close) {
                $mdDialog.hide();
            });
        }
        
        this.close = function(){
            $mdDialog.hide();
        }
        
        
        
    }
    
    
    var contactController = function($scope,Service,$log){
        var ctrl = this;
        var contactForm = ctrl.user;
        
        ctrl.feedback = function(){
            $log.info(ctrl.user);
            $log.info(angular.toJson(ctrl.user));
        }
    }
    var videoController = function($scope,Service,$log){
        
        var videoCtrl = this;
        Service.getVideos().success(function(data){
            videoCtrl.videoList = data;
        }).error(function(){
            console.log('error occured ... ');
        });
        
        $scope.getIframeSrc = function(src) {
            return 'https://www.youtube.com/embed/'+src+'?autoplay=0&origin=http://www.signinevents.com';
        };
        
        
    }
    
    var homeController = function($scope,$log){
        
        $log.info('Inside of homeController ... ');
        var hCtrl = this;
        hCtrl.initialize = function(){
            
                var latlng = new google.maps.LatLng(17.350639, 78.507855);

                var myOptions = {
                    zoom: 18,
                    center: latlng,
                    panControl: true,
                    zoomControl: true,
                    scaleControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
               var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                var marker = new google.maps.Marker({
                    map: map,
                    draggable:false,	
                    position: latlng
                });
                google.maps.event.addListener(marker, 'dragend', function() {


                    var point = marker.getPosition();

                    map.panTo(point);
                    // Update the textbox
                    document.getElementById('txt_latlng').value=point.lat()+", "+point.lng();
                    var latlong = new google.maps.LatLng(point.lat(), point.lng());
                    //getAddress(latlong);

                });
        }
        hCtrl.initialize();
    }
    
    imageController.inject = ['$scope','Service','$log','$mdDialog'];
    contactController.inject = ['$scope','Service','$log'];
    videoController.inject = ['$scope','Service','$log'];
    homeController.inject = ['$scope','$log'];
    
    angular.module('signinApp').controller('ImageCtrl',imageController)
    .controller('ContactCtrl',contactController)
    .controller('videoCtrl', videoController)
    .controller('homeCtrl',homeController);
    
})(window.angular);